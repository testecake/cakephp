<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
  $this->assign('title', 'Cadastro de usuário');
?>
<body>
<div id="topo">
</div>
<nav class="large-2 medium-1 columns" id="sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Listar usuários'), ['action' => 'viewUsers']) ?> </li>
    </ul>
</nav>
<div id="cadastro">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Novo usuário') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('username');
            echo $this->Form->control('email');
            echo $this->Form->control('password');
            echo $this->Form->control('roles_id', ['options' => $roles]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Html->link(__('Voltar ao painel'), ['action' => 'index']) ?>
    <?= $this->Form->end() ?>
</div>
</body>
