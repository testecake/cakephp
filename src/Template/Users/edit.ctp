<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
$this->assign('title', 'Edição usuário');
?>
<div id="topo">

</div>
<div id="conteudo">
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(
                __('Deletar usuário'),
                ['action' => 'delete', $user->id],
                ['confirm' => __('Você tem certeza que quer deletar o usuário # {0}?', $user->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listar usuários'), ['action' => 'viewUsers']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Edit User') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('username');
            echo $this->Form->control('email');
            echo $this->Form->control('password');
            echo $this->Form->control('roles_id', ['options' => $roles]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Html->link(__('Voltar ao painel'), ['action' => 'index']) ?>
    <?= $this->Form->end() ?>
</div>
</div>