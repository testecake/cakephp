<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
  $this->assign('title', 'Gerenciamento do sistema');
?>
<body>
<div id="topo">

</div>
<div id="conteudo">
<div id="acoes">
    <ul>
            
            <li><img src="http://localhost/Projeto/SistemaCake/webroot/img/usuario.png" onMouseOver="this.src='http://localhost/Projeto/SistemaCake/webroot/img/usuario_hover.png'" onMouseOut="this.src='http://localhost/Projeto/SistemaCake/webroot/img/usuario.png'"></li>
            <li><img src="http://localhost/Projeto/SistemaCake/webroot/img/l_usuario.png" onMouseOver="this.src='http://localhost/Projeto/SistemaCake/webroot/img/l_user_hover.png'" onMouseOut="this.src='http://localhost/Projeto/SistemaCake/webroot/img/l_usuario.png'"></li>
            <li><img src="http://localhost/Projeto/SistemaCake/webroot/img/cargo.png" onMouseOver="this.src='http://localhost/Projeto/SistemaCake/webroot/img/cargo_hover.png'" onMouseOut="this.src='http://localhost/Projeto/SistemaCake/webroot/img/cargo.png'"></li>
            <li><img src="http://localhost/Projeto/SistemaCake/webroot/img/l_cargo.png" onMouseOver="this.src='http://localhost/Projeto/SistemaCake/webroot/img/l_cargo_hover.png'" onMouseOut="this.src='http://localhost/Projeto/SistemaCake/webroot/img/l_cargo.png'"></li>
               
    </ul>
</div>
<div id="descricao">
    <ul>
        <li><?= $this->Html->link(__('Novo usuário'), ['action' => 'add']) ?></li>
        <li class="movimenta"><?= $this->Html->link(__('Listar usuários'), ['action' => 'viewUsers']) ?></li>
        <li class="movimenta"><?= $this->Html->link(__('Novo cargo'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
        <li class="movimenta"><?= $this->Html->link(__('Listar cargos'), ['controller' => 'Roles', 'action' => 'index']) ?></li>
    </ul>
</div>
</body>
