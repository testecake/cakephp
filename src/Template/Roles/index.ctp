<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Role[]|\Cake\Collection\CollectionInterface $roles
 */
$this->assign('title', 'Listagem de cargos');
?>
<div id="topo">
</div>
    <nav class="large-2 medium-4 columns" id="sidebar">
        <ul class="side-nav">
            <li class="heading"><?= __('Ações') ?></li>
            <li><?= $this->Html->link(__('Novo cargo'), ['controller' => 'Roles', 'action' => 'add']) ?></li>
        </ul>
    </nav>
    <h3 class="ti"><?= __('Consulta de cargos cadastrados') ?></h3>
    <div class="tabela">
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('Id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Cargo') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Criado') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Modificado') ?></th>
                <th scope="col" class="actions"><?= __('Ações') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($roles as $role): ?>
            <tr>
                <td><?= $this->Number->format($role->id) ?></td>
                <td><?= h($role->role) ?></td>
                <td><?= h($role->created) ?></td>
                <td><?= h($role->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Visualizar'), ['action' => 'view', $role->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $role->id]) ?>
                    <?= $this->Form->postLink(__('Deletar'), ['action' => 'delete', $role->id], ['confirm' => __('Você tem certrza que quer excluir o cargo # {0}?', $role->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Primeira')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Próxima') . ' >') ?>
            <?= $this->Paginator->last(__('Última') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} de {{count}}')]) ?></p>
        <span class="voltar"><?= $this->Html->link(__('Voltar ao painel'), ['controller' => 'users', 'action' => 'index']) ?></span>
    </div>
</div>
