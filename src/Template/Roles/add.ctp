<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Role $role
 */
$this->assign('title', 'Cadastro de cargo');
?>
<div id="topo">
</div>
<nav class="large-2 medium-1 columns" id="sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Listar cargos'), ['controller' => 'Roles', 'action' => 'index']) ?> </li>
    </ul>
</nav>
<div id="cadastro">
    <?= $this->Form->create($role) ?>
    <fieldset>
        <legend><?= __('Adicionar cargo') ?></legend>
        <?php
            echo $this->Form->control('role');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <span class="voltar"><?= $this->Html->link(__('Voltar ao painel'), ['controller' => 'users', 'action' => 'index']) ?></span>
    <?= $this->Form->end() ?>
</div>
