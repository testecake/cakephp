<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Role $role
 */
    $this->assign('title', 'Consulta de cargo');
?>
<div id="topo">

</div>
<div id="conteudo">
<nav class="large-3 medium-4 columns" id="sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Html->link(__('Editar cargo'), ['action' => 'edit', $role->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Deletar cargo'), ['action' => 'delete', $role->id], ['confirm' => __('Você tem certrza que quer excluir o cargo # {0}?', $role->id)]) ?> </li>
        <li><?= $this->Html->link(__('Listar cargos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Novo cargo'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="roles view large-9 medium-8 columns content">
    <h3><?= h($role->role) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Role') ?></th>
            <td><?= h($role->role) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($role->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($role->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($role->modified) ?></td>
        </tr>
    </table>
    <?= $this->Html->link(__('Voltar ao painel'), ['controller' => 'users', 'action' => 'index']) ?>
</div>
