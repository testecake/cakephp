<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Role $role
 */
$this->assign('title', 'Edição de cargo');
?>
<div id="topo">

</div>
<div id="conteudo">
<nav class="large-3 medium-4 columns" id="sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Ações') ?></li>
        <li><?= $this->Form->postLink(
                __('Deletar cargo'),
                ['action' => 'delete', $role->id],
                ['confirm' => __('Você tem certeza que quer deletar o cargo # {0}?', $role->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Listar cargos'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="roles form large-9 medium-8 columns content">
    <?= $this->Form->create($role) ?>
    <fieldset>
        <legend><?= __('Editar cargo') ?></legend>
        <?php
            echo $this->Form->control('role');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Enviar')) ?>
    <?= $this->Html->link(__('Voltar ao painel'), ['controller' => 'users', 'action' => 'index']) ?>
    <?= $this->Form->end() ?>
</div>
